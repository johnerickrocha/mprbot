
![N|derr](https://upload.wikimedia.org/wikipedia/en/0/00/IBM_Watson_Logo_2017.png)

# MPRbot 0.1.0

Aplicação de conversação com Inteligência Artificial ***Watson***.

Consulta de treino para usuários do app MPR.

### Instalação

Watson requer [Node.js](https://nodejs.org/) v4+ para funcionar.

Instale as dependências e start server.

```sh
$ cd mprbot/
$ npm install 
$ node app.js
```

### Configuração Credenciais Watson

Em [Bluemix](https://www.ibm.com/br-pt/marketplace/cloud-platform?S_PKG=&cm_mmc=Search_Google-_-Cloud_Platform-_-BR_BR-_-+bluemix_Broad_&cm_mmca1=000008ON&cm_mmca2=10001882&mkwid=66d32e27-4172-4dde-a809-aa232eb8a803|956|54081&cvosrc=ppc.google.%2Bbluemix&cvo_campaign=Cloud_Platform-BR_BR&cvo_crid=215316421640&Matchtype=b), crie uma conta e crie o serviço [Conversation Watson](https://www.ibm.com/watson/services/conversation-2/).

Vá em **Credenciais** para obter as chaves de acesso.

```sh
var username =    '<yourwatsonconversationusername>';
var password =    '<yourpassword>'
var workspace_id = '<yourwatsonconversationworkspaceid>';
```


### Configuração Credenciais Facebook

Acesse [Área de Desenvolvedor do Facebook](https://developers.facebook.com/) e crie um aplicativo.

Crie e vincule o aplicativo a uma página do Facebook.

Selecione uma Página, para geração do ***Token*** e insira no código

```sh
var token = "<yourfacebookweebhooktoken>"
```

Acesse *tipos de aplicativos*, selecione **Messenger** → ***Webhooks***

Crie uma senha para verificar o ***token*** de seu Aplicativo Messenger.


    app.get('/webhook', function(req,res){
        if (req.query['hub.mode'] === 'subscribe' && req.query['hub.verify_token'] === '<createyourwebookpasswordverification>'){ // Crie 
            console.log('Enviando...');
            res.status(200).send(req.query['hub.challenge']);
        } else {
            console.log('Get Facebook Webhook not send');
            res.sendStatus(403);
        }
    });

Insira a URL do Server no campo Webhooks no Painel do Aplicativo Messenger para assinatura.

###### URL de retorno de chamada
```sh
https://sleepy-anchorage-68492.herokuapp.com/webhook 
```
###### Verificar token
```sh
createyourwebookpassword
```

### Configuração de Alocação do Servidor

Esta aplicação usa o servidor de alocação **Heroku**
 > Consulte a documentação → [Heroku NodeJS](https://devcenter.heroku.com/categories/nodejs) 

Crie uma conta em [Heroku](https://www.heroku.com/) e selecione ***NodeJS*** 

Crie uma acesso remoto para alocar seu servidor no servidor cloud *heroku*

```sh
$ cd mprbot
$ heroku create
$ git init
$ git add .
$ git commit -m "comment here"
$ git heroku push master
```

### JSON REST API request-promise

Esta aplicação usa [request-promise](https://www.npmjs.com/package/request-promise)
 > Consulte a  documentação → [request-promise](https://github.com/request/request-promise)
 
##### GET JSON REST API

Sample para invocar **/GET**

    var options = {
    uri: 'https://api.github.com/user/repos',
        qs: {
            access_token: 'xxxxx xxxxx' // -> uri + '?access_token=xxxxx%20xxxxx'
        },
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true 
    }; 
    
    rp(options)
        .then(function (repos) {
            console.log('User has %d repos', repos.length);
        })
    .catch(function (err) {
        // API call failed...
        });

## Noção do Serviço **Watson Conversation**

Em seu **Dashboard** do ***Console Bluemix Watson Conversation***

Crie um **Dialog** → ***Node*** → ***Open JSON editor***

- **Dialog** → ***Node***
![N|derr](https://developer.ibm.com/answers/storage/temp/15894-bug.png)

- **Node** → ***Open JSON editor***
 ![N|derr](https://i0.wp.com/developer.ibm.com/in/wp-content/uploads/sites/115/2017/04/WatsonConversationSnippet1-1.png?ssl=1)

##### Crie um **context**

Em *JSON editor* crie um *Objeto* → ***"context: "*** para armazenar o *input* do Usuário

```sh
{
  "context": {
    "email": "<armazeneumavariavellocal>" 
  }, // use a variável em seu server - context.email
  "output": {
    "text": {
      "values": [
        "Legal, agora preciso da sua senha 🔐"
      ],
      "selection_policy": "sequential"
    }
  }
}
```

##### Use JSON Workspace

Importe o arquivo de configuração de JSON do Watson Conversation **Workspace**
